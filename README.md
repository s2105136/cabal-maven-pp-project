# PP cabal + maven boilerplate

This repository is a template for people wanting to use cabal + maven instead of stack + maven.
It does require a relatively recent version of cabal (version 2.4, so not *that* recent), but if you are trying to run this from GHC 8.0.2, chances are you might want to update

For the java part of the project, see my-lang-java. It has been yoinked from the set of boilerplates Arnd published on canvas

to build everything haskell-related, just do `cabal build`. A sample my-lang-haskell-project should produce an executable that can parse SprIL files and run them in sprockell. So if you don't want to
use haskell after all, you still have an executable you can use. You can find it as `./my-lang-haskell-exe` (its otherwise hidden quite deeply in you build dir

If you want open the project in a repl just type `cabal repl my-lang-haskell` and it will automatically build all dependencies if they changed

The "my lang" name is a placeholder, and I, just like the creators of all the other boilerplates, encourage you to come up with your own name.
