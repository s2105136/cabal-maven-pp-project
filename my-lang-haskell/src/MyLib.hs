module MyLib (compileAndRun) where

import Sprockell
import Text.Read (readMaybe)
import System.IO (hPutStrLn, stderr)

compileAndRun :: String -> Int -> IO ()
compileAndRun src threadCount = case prog of
                                  Nothing -> hPutStrLn stderr "Could not parse sprockell source"
                                  Just p -> run $ replicate threadCount p
  where
  prog :: Maybe [Instruction]
  prog = readMaybe src


