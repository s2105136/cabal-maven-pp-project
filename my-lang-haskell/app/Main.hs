module Main where

import MyLib
import System.IO (hPutStrLn, stderr)
import System.Environment (getArgs)
import Text.Read (readMaybe)

main :: IO ()
main = do
  args <- getArgs
  stringPair <- case args of
                   [filename, threadcount] -> pure $ Just (filename, threadcount)
                   _ -> (hPutStrLn stderr "Usage: [sprockell filename] [thread count]") >> pure Nothing
  let (iosrc, threadcount) = maybe (error "Parsing fail") id $ parsePair stringPair
  src <- iosrc
  compileAndRun src threadcount
  


parsePair :: Maybe (FilePath, String) -> Maybe (IO String, Int)
parsePair inp = do
  (filename, stringThreads) <- inp
  threadCount <- readMaybe stringThreads
  let src = readFile filename
  return (src, threadCount)
