# The haskell part of your integrating project

This is a cabal package. It is mainly divided into 2 parts

- `app/` is where the executable part of your code lives. Here is where you are supposed to mess with IO Functors/Applicatives/Monads and handle user input  
  It is convention to not put any of your advanced application logic here, that goes into `src/`
- `src/` is where the brunt of your compiler-related code is supposed to live (think of codegen and all that). If you want to use multiple modules in the `app/` part of  
  your package, don't forget to **put them in the exposed-modules part of the "library" section of your .cabal file**.
